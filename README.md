# BetVictor action-monitor

## How to build + run
We can use docker in order to build and run this app. For ease of use the dockerfile copies the contents of the application, builds the jar and runs the application

Here are the steps we should follow (we should first go to the project's directory):
* `docker build -t bv-am .`
* `docker run -d --name bv-app -p 8080:8080`

Then we can access the application from [here](http://localhost:8080/)

To test the solution we first need to create two users. To do that we can access the swagger UI
from [here](http://localhost:8080/swagger-ui/index.html) and do a **POST** request in the **/users** endpoint.

Once we created two users we can refresh the application. We will notice that the usernames we gave to the users are now available in two dropdown options.

Let's open two windows, so we can test the chatting functionality. Please choose the users as sender or receiver.
In one of the two windows send a message to the receiver. We will notice that the message is also displayed in the other window!

Note that we can see all the users and messages stored in the database under [here](http://localhost:8080/h2-console)

We will need to change the jdbc url to: **jdbc:h2:mem:action-monitor**

To check if the application is up and running and to find some information about the application version we can access:

* http://localhost:8080/actuator/health
* http://localhost:8080/actuator/info

## Future improvements
* Add tests for the web sockets
* Add a chatroom functionality. With the current implementation a user when subscribing will receive messages from all the users that might send him a message.