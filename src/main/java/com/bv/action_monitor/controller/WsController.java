package com.bv.action_monitor.controller;

import com.bv.action_monitor.dto.MessageDto;
import com.bv.action_monitor.dto.UserMessageResponseDto;
import com.bv.action_monitor.entity.UserMessage;
import com.bv.action_monitor.mapper.UserMessageMapper;
import com.bv.action_monitor.service.UserMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import javax.transaction.Transactional;

import static com.bv.action_monitor.ActionMonitorApplication.logger;

@Controller
public class WsController {

    @Autowired
    private UserMessageMapper userMessageMapper;

    @Autowired
    private UserMessageService userMessageService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @MessageMapping("/chat")
    public void processMessage(@Payload MessageDto message) {
        UserMessage createdMessage = userMessageService.saveMessage(message);
        logger.info("User message was created successfully");
        UserMessageResponseDto userMessageResponseDto = userMessageMapper.toUserMessageResponseDto(createdMessage);
        simpMessagingTemplate.convertAndSendToUser(message.getReceiverId().toString(), "/queue/messages", userMessageResponseDto);
    }

}
