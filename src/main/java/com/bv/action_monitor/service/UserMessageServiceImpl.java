package com.bv.action_monitor.service;

import com.bv.action_monitor.dto.MessageDto;
import com.bv.action_monitor.entity.User;
import com.bv.action_monitor.entity.UserMessage;
import com.bv.action_monitor.exceptions.UserDoesNotExistException;
import com.bv.action_monitor.repository.UserMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserMessageServiceImpl implements UserMessageService {

    @Autowired
    UserMessageRepository userMessageRepository;

    @Autowired
    UserService userService;

    @Override
    public UserMessage saveMessage(MessageDto messageDto) {
        User sender = userService.getUserById(messageDto.getSenderId()).orElseThrow(() -> new UserDoesNotExistException(messageDto.getSenderId()));
        User receiver = userService.getUserById(messageDto.getReceiverId()).orElseThrow(() -> new UserDoesNotExistException(messageDto.getReceiverId()));

        return userMessageRepository.save(UserMessage.builder().sender(sender).receiver(receiver).message(messageDto.getMessageContent()).build());
    }

    @Override
    public List<UserMessage> getConversation(long senderId, long receiverId) {
        List<Long> userIds = List.of(senderId, receiverId);
        return userMessageRepository.getUserMessageBySenderIdInAndReceiverIdInOrderByCreatedAt(userIds, userIds);
    }
}
