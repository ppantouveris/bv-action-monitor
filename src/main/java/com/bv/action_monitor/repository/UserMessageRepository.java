package com.bv.action_monitor.repository;

import com.bv.action_monitor.entity.UserMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMessageRepository extends JpaRepository<UserMessage, Long> {

    List<UserMessage> getUserMessageBySenderIdInAndReceiverIdInOrderByCreatedAt(List<Long> userIds, List<Long> userIdsList);

}
