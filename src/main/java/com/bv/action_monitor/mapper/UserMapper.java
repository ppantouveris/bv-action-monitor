package com.bv.action_monitor.mapper;

import com.bv.action_monitor.dto.UserDto;
import com.bv.action_monitor.entity.User;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserMapper {

    @Autowired
    private ModelMapper modelMapper;

    public UserDto toUserDto(User user) {
        return modelMapper.map(user, UserDto.class);
    }

    public User toUser(UserDto userDto) {
        return modelMapper.map(userDto, User.class);
    }

    public List<UserDto> userDtoList(List<User> userList) {
        return userList.stream().map(this::toUserDto).collect(Collectors.toList());
    }
}
