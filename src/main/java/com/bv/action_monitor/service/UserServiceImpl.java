package com.bv.action_monitor.service;

import com.bv.action_monitor.entity.User;
import com.bv.action_monitor.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.bv.action_monitor.ActionMonitorApplication.logger;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User createUser(User user) {
        User createdUser = userRepository.save(user);
        logger.info("User %s was created successfully".formatted(createdUser.getName()));

        return createdUser;
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public Optional<User> getUserById(long userId) {
        return userRepository.findById(userId);
    }
}
