package com.bv.action_monitor.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserMessageResponseDto {
    private String senderName;
    private String receiverName;
    private String messageContent;
    private LocalDateTime createdAt;
}
