package com.bv.action_monitor.exceptions;

public abstract class NotFoundException extends RuntimeException {

    public NotFoundException(String s) {
        super(s);
    }

    public abstract String getError();
}
