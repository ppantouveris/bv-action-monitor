FROM maven:3.8.5-openjdk-18-slim

COPY . /tmp/app

WORKDIR /tmp/app

RUN mvn clean install

RUN mkdir /app

RUN mv /tmp/app/target/bv-action-monitor.jar /app/bv-action-monitor.jar

EXPOSE 8080

WORKDIR /app

# the extra options are needed for h2-console access
ENTRYPOINT [ "java", "-jar", "bv-action-monitor.jar", "-web -webAllowOthers -tcp -tcpAllowOthers -browser" ]