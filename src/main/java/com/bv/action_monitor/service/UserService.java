package com.bv.action_monitor.service;

import com.bv.action_monitor.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    User createUser(User user);

    List<User> getAllUsers();

    Optional<User> getUserById(long userId);
}
