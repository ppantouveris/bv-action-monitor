package com.bv.action_monitor.exceptions;

public class UserDoesNotExistException extends NotFoundException {

    public UserDoesNotExistException(long userId) {
        super("User with id %s does not exist".formatted(userId) );
    }

    @Override
    public String getError() {
        return "User.NotFound";
    }
}
