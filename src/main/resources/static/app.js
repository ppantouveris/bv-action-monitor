let stompClient = null;
let subscription = null;

const connect = () => {
    let socket = new SockJS('/bv-action-monitor-ws');
    $("#wsStatus").append("<h2>Sockets connection is on</h2>")
    stompClient = Stomp.over(socket);
    stompClient.connect({}, subscribeUser);
}

const displayMessages = (messages) => {
    let html = ""
    if (Array.isArray(messages)) {
        messages.forEach(message => {
            html += "<tr><td>" + `${message.senderName} says: ${message.messageContent}` + "</td></tr>"
        })
    } else {
        html += "<tr><td>" + `${messages.senderName} says: ${messages.messageContent}` + "</td></tr>"
    }

    $("#conversationHistory").append(html);
}

const sendMessage = () => {
    stompClient.send("/app/chat", {}, JSON.stringify({
                'senderId': parseInt($("#selectUserSender :selected").val()),
                'receiverId': parseInt($("#selectUserReceiver :selected").val()),
                'messageContent': $("#messageContent").val(),
            }
        )
    );

    displayMessages({
        'senderName': $("#selectUserSender :selected").text(),
        'messageContent': $("#messageContent").val()
    });
}

const fetchAsync = async (url) => {
    let response = await fetch(url);
    return await response.json();
}

const fetchConversation = (senderId, receiverId) => {
    fetchAsync(`http://localhost:8080/users/messages?senderId=${senderId}&receiverId=${receiverId}`)
        .then(r => {
            $("#conversation > tbody").empty();
            displayMessages(r.data)
        });
}

const subscribeUser = () => {
    subscription = stompClient.subscribe("/user/" + $("#selectUserSender :selected").val() + "/queue/messages", (message) => {
        displayMessages(JSON.parse(message.body))
    });
}

$(() => {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $("#send").click(function () {
        sendMessage();
    });

    connect();

    fetchAsync("http://localhost:8080/users/all").then(r => {
        let htmlResult = "";
        r.forEach(user => {
            htmlResult += '<option value="' + user.id + '">' + user.name + '</option>';
        })
        $("#selectUserSender").append(htmlResult);
        $("#selectUserReceiver").append(htmlResult);
    });

    $("#selectUserSender").change(() => {
        let prev = $("#selectUserSender").data('val');
        let current = $("#selectUserSender").val();
        subscription.unsubscribe()
        fetchConversation(parseInt($("#selectUserSender :selected").val()), parseInt($("#selectUserReceiver :selected").val()));
        subscribeUser();
    });

    $('#selectUserSender').on('focusin', function () {
        console.log("Saving value " + $(this).val());
        $(this).data('val', $(this).val());
    });

    $("#selectUserReceiver").change(() => {
        fetchConversation(parseInt($("#selectUserSender :selected").val()), parseInt($("#selectUserReceiver :selected").val()));
    });
});

