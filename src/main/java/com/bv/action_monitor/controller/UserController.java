package com.bv.action_monitor.controller;

import com.bv.action_monitor.dto.ResponseDataWrapper;
import com.bv.action_monitor.dto.UserDto;
import com.bv.action_monitor.dto.UserMessageResponseDto;
import com.bv.action_monitor.entity.User;
import com.bv.action_monitor.entity.UserMessage;
import com.bv.action_monitor.mapper.UserMapper;
import com.bv.action_monitor.mapper.UserMessageMapper;
import com.bv.action_monitor.service.UserMessageService;
import com.bv.action_monitor.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserMessageService userMessageService;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserMessageMapper userMessageMapper;

    @PostMapping
    @ResponseBody
    public ResponseEntity<UserDto> createUser(@RequestBody UserDto userDto) {
        User user = userMapper.toUser(userDto);
        User createdUser = userService.createUser(user);
        UserDto createdUserDto = userMapper.toUserDto(createdUser);

        return new ResponseEntity<>(createdUserDto, HttpStatus.OK);
    }

    @ResponseBody
    @GetMapping(path = "/all")
    public ResponseEntity<List<UserDto>> getAllUsers() {
        List<User> allUsers = userService.getAllUsers();
        List<UserDto> allUserDto = userMapper.userDtoList(allUsers);

        return new ResponseEntity<>(allUserDto, HttpStatus.OK);
    }

    @ResponseBody
    @GetMapping(path = "/messages")
    public ResponseEntity getConversation(@RequestParam long senderId, @RequestParam long receiverId) {
        List<UserMessage> userMessageList = userMessageService.getConversation(senderId, receiverId);
        List<UserMessageResponseDto> userMessageResponseDtoList = userMessageMapper.toUserMessageResponseDtoList(userMessageList);

        return new ResponseEntity<>(ResponseDataWrapper.builder().data(userMessageResponseDtoList).count(userMessageResponseDtoList.size()).build(), HttpStatus.OK);
    }

}
