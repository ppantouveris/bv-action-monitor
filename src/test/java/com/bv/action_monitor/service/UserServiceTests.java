package com.bv.action_monitor.service;

import com.bv.action_monitor.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

@SpringBootTest
public class UserServiceTests {

    @Autowired
    private UserService subject;

    @Test
    public void shouldCreateUser() {
        // Given
        String userName = "testUser";
        User user = User.builder().name(userName).build();

        // When
        User createdUser = subject.createUser(user);

        // Then
        assertThat(createdUser.getId(), is(notNullValue()));
    }
}
