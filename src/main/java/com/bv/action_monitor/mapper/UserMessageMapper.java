package com.bv.action_monitor.mapper;

import com.bv.action_monitor.dto.UserMessageResponseDto;
import com.bv.action_monitor.entity.UserMessage;
import org.hibernate.Session;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserMessageMapper {

    public UserMessageResponseDto toUserMessageResponseDto(UserMessage userMessage) {
        return UserMessageResponseDto.builder().senderName(userMessage.getSender().getName())
                .receiverName(userMessage.getReceiver().getName())
                .messageContent(userMessage.getMessage())
                .createdAt(userMessage.getCreatedAt())
                .build();
    }

    public List<UserMessageResponseDto> toUserMessageResponseDtoList(List<UserMessage> userMessageList) {
        return userMessageList.stream().map(this::toUserMessageResponseDto).collect(Collectors.toList());
    }
}
