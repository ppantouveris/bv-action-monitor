package com.bv.action_monitor.service;

import com.bv.action_monitor.dto.MessageDto;
import com.bv.action_monitor.entity.User;
import com.bv.action_monitor.entity.UserMessage;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@SpringBootTest
public class UserMessageServiceTests {

    @Autowired
    private UserMessageService subject;

    @Autowired
    private UserService userService;

    private User testSenderUser;

    private User testReceiverUser;

    @BeforeEach
    public void before() {
        testSenderUser = testSenderUser == null ? userService.createUser(User.builder().name("sender").build()) : testSenderUser;
        testReceiverUser = testReceiverUser == null ? userService.createUser(User.builder().name("receiver").build()) : testReceiverUser;
    }

    @Test
    public void shouldSaveMessage() {
        // Given
        String messageContent = "test";
        MessageDto messageDto = MessageDto.builder().receiverId(testReceiverUser.getId())
                .senderId(testSenderUser.getId())
                .messageContent(messageContent)
                .build();

        // When
        UserMessage userMessage = subject.saveMessage(messageDto);

        // Then
        assertThat(userMessage.getId(), is(notNullValue()));
        assertThat(userMessage.getMessage(), equalTo(messageContent));
    }

    @Test
    public void shouldGetConversation(){
        // Given
        String messageContent = "test";
        MessageDto messageDto = MessageDto.builder().receiverId(testReceiverUser.getId())
                .senderId(testSenderUser.getId())
                .messageContent(messageContent)
                .build();
        subject.saveMessage(messageDto);

        // When
        List<UserMessage> userMessages = subject.getConversation(testSenderUser.getId(), testReceiverUser.getId());

        // Then
        assertThat(userMessages.size(), equalTo(1));
        assertThat(userMessages.get(0).getMessage(), equalTo(messageContent));

    }
}
