package com.bv.action_monitor.service;

import com.bv.action_monitor.dto.MessageDto;
import com.bv.action_monitor.entity.UserMessage;

import java.util.List;

public interface UserMessageService {
    UserMessage saveMessage(MessageDto messageDto);

    List<UserMessage> getConversation(long senderId, long receiverId);
}
